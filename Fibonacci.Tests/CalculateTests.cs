using Xunit;
using static Fibonacci.ClassLibrary.Sequence;

namespace Fibonacci.Tests
{
    public class CalculateTests
    {
        [Fact]
        public void CalculateAtTest()
        {
            // 0 1 2 3 4 5 6  7  8  9 10 11
            // 0 1 1 2 3 5 8 13 21 34 55 89
            Assert.Equal(0, CalculateAt(0));
            Assert.Equal(1, CalculateAt(1));
            Assert.Equal(1, CalculateAt(2));
            Assert.Equal(2, CalculateAt(3));
            Assert.Equal(3, CalculateAt(4));
            Assert.Equal(5, CalculateAt(5));
            Assert.Equal(8, CalculateAt(6));
            Assert.Equal(13, CalculateAt(7));
            Assert.Equal(21, CalculateAt(8));
            Assert.Equal(34, CalculateAt(9));
            Assert.Equal(55, CalculateAt(10));
        }

        [Fact]
        public void GetSequenceTest()
        {
            Assert.True("0".CompareTo(GetSequence(0)) == 0);
            Assert.True("0 1".CompareTo(GetSequence(1)) == 0);
            Assert.True("0 1 1 2".CompareTo(GetSequence(3)) == 0);
            Assert.True("0 1 1 2 3 5 8 13 21 34 55 89".CompareTo(GetSequence(11)) == 0);
        }
    }
}
