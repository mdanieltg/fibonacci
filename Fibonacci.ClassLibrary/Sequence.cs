﻿using System;

namespace Fibonacci.ClassLibrary
{
    public static class Sequence
    {
        public static long CalculateAt(int position)
        {
            if (position < 0) throw new ArgumentOutOfRangeException(nameof(position));

            long result = 0, lastResult = 0;

            for (var i = 1; i <= position; i++)
            {
                var last = result;

                if (result == 0)
                    result += 1;
                else
                    result += lastResult;

                lastResult = last;
            }

            return result;
        }

        public static string GetSequence(int length)
        {
            if (length < 0) throw new ArgumentOutOfRangeException(nameof(length));

            var stringBuilder = new System.Text.StringBuilder();
            long result = 0, lastResult = 0;

            stringBuilder.Append("0 ");

            for (var i = 1; i <= length; i++)
            {
                var last = result;

                if (result == 0)
                    result += 1;
                else
                    result += lastResult;

                stringBuilder.Append(result);
                stringBuilder.Append(' ');

                lastResult = last;
            }

            return stringBuilder.ToString().Trim();
        }

        public static string GetSequence(int start, int length)
        {
            if (start < 0) throw new ArgumentOutOfRangeException(nameof(start));
            if (length < 1) throw new ArgumentOutOfRangeException(nameof(length));

            throw new NotImplementedException();
        }
    }
}
